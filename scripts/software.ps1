#Script to install software via Chocolatey on the Winserver VM

#Software to install
$software = "7zip"
#Install choclatey
Write-Host "Installing Chocolatey"
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')) | Out-Null
#Reload profile
&$profile

#Enable global confirmation in chocoaltey to be able to automatically install software
choco feature enable -n allowGlobalConfirmation

foreach ($i in $software){
    Write-Host "Installing $i"
    choco install $i | Out-Null
}


